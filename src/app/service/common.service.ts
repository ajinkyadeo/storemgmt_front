import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public config = environment;
  constructor(private http: HttpClient, private snackbar: MatSnackBar) { }
  userSession: any;

  showSnackBar(type: string, message: string, duration?: number) {
    if (duration == null)
      duration = 10000;
    let matSnackbarConfig: MatSnackBarConfig = {
      panelClass: 'bg-' + type,
      duration: duration,
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    };
    this.snackbar.open(message, 'Dismiss', matSnackbarConfig);
  }

  fillSession() {
    var storage: any = localStorage.getItem('userSessionData')
    if (this.userSession == undefined)
      this.userSession = JSON.parse(storage);
  }

  userlogin(email: string, password: string): Observable<any> {

    return this.http.get<any>(this.config.url + '/Users/Login/' + email + '/' + password)

  }

  userlist(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Users/GetAllUsers', {})
  }

  GetAllProduct(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Product/GetAllProduct', {})
  }

  GetAllDistributor(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Distributor/GetAllDistributor', {})
  }

  GetAllDistributorByProduct(id: any): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Distributor/GetAllDistributorByProduct/' + id, {})
  }

  GetAllInventorys(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Inventorys/GetAllInventorys', {})
  }

  GetAllSaleOrders(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/SaleOrders/GetAllSaleOrders', {})
  }

  GetAllCustomers(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Customers/GetAllCustomers', {})
  }

  GetAllSetting(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/MstSetting/GetAllSetting', {})
  }

  CreateInventory(inventory: any): Observable<any> {
    return this.http.post<any>(this.config.url + '/Inventorys/CreateInventory', inventory)
  }

  GetUserById(id: number): Observable<any> {
    return this.http.get<any>(this.config.url + '/Users/GetUserById/' + id)
  }

  GetAllRoleMenus(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Menu/GetMenuByRoleId/' + JSON.parse(localStorage.userSessionData)[0].RoleId, {})
  }

  GetRoleMenuByRoleId(): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/RoleMenus/GetRoleMenuByRoleId/' + JSON.parse(localStorage.userSessionData)[0].RoleId, {})
  }

  CreateSaleOrder(salses: any): Observable<any> {
    return this.http.post<any>(this.config.url + '/SaleOrders/CreateSaleOrder', salses)
  }
  GetByProductId(id: number): Observable<any> {
    return this.http.get<any>(this.config.url + '/Inventorys/GetByProductId/' + id, {});
  }
  GetAllInventorysByProductId(id: any, distId: any): Observable<any[]> {
    return this.http.get<any[]>(this.config.url + '/Inventorys/GetAllInventorysByProductId/' + id + '/' + distId, {})
  }

  UpdatePassword(obj: any): Observable<any> {
    return this.http.post<any>(this.config.url + '/Users/UpdatePassword', obj)
  }

  UpdateUser(user: any): Observable<any> {
    return this.http.post<any>(this.config.url + '/Users/UpdateUser', user)
  }

  UpdateSaleOrder(user: any): Observable<any> {
    return this.http.post<any>(this.config.url + '/SaleOrders/UpdateSaleOrder', user)
  }

  GetDeleteSaleOrder(id: number): Observable<any> {
    return this.http.get<any>(this.config.url + '/SaleOrders/GetDeleteSaleOrder/' + id)
  }

  LockUser(userId: number, isLock: boolean): Observable<any> {
    return this.http.get<any>(this.config.url + '/Users/LockUser/' + userId + '/' + isLock)
  }

  GetAllRole(): Observable<any> {
    return this.http.get<any>(this.config.url + '/Role/GetAllRole')
  }

}
