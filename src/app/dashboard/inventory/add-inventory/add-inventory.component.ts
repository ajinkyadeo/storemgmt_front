import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-add-inventory',
  templateUrl: './add-inventory.component.html',
  styleUrls: ['./add-inventory.component.scss']
})
export class AddInventoryComponent implements OnInit {


  fg: FormGroup;
  products: any[] = [];
  distributor: any[] = [];
  state: string = "add";
  constructor(
    public dialogRef: MatDialogRef<AddInventoryComponent>
    , private fb: FormBuilder,
    private cs: CommonService) {
    // this.cs.fillSession();
    this.fg = this.fb.group({
      InventoryId: [''],
      ProductId: [''],
      DistributorId: [''],
      Units: [''],
      DistributorPrice: [''],
      DeliveredOn: ['']
    })
  }

  ngOnInit() {
    this.GetAllProduct();
    this.GetAllDistributor();
  }

  get f() {
    return this.fg;
  }

  onchangeProduct(event: any) {
  }

  GetAllDistributor() {
    this.cs.GetAllDistributor().subscribe((distributor: any) => {
      if (distributor.ReturnObject !== null) {
        this.distributor = distributor.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });

  }

  GetAllProduct() {
    this.cs.GetAllProduct().subscribe((product: any) => {
      if (product.ReturnObject !== null) {
        this.products = product.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });

  }

  onclick() {
    var inventory: any = this.fg.value;
    this.cs.CreateInventory(inventory).subscribe((res: any) => {

      if (res.isError == 1) {
        this.cs.showSnackBar('danger', res.DetailsError);
      } else {
        this.cs.showSnackBar('success', 'Record Added successfully')
      }
      this.dialogRef.close();

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured : Record Not Created ');
    })
  }

  cancel() {
    this.dialogRef.close();
  }

}
