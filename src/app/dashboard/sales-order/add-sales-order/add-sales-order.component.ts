import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-add-sales-order',
  templateUrl: './add-sales-order.component.html',
  styleUrls: ['./add-sales-order.component.scss']
})
export class AddSalesOrderComponent implements OnInit {
  fg: FormGroup;
  distributor: any[] = [];
  products: any[] = [];
  sales: any[] = [];
  Customer: any[] = [];
  AllUnit: any[] = [];
  MRP: any = 0;
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router,
    private fb: FormBuilder, public dialogRef: MatDialogRef<AddSalesOrderComponent>) {
    this.fg = this.fb.group({
      Id: '',
      CustomerId: '',
      ProductId: '',
      DistributorId: '',
      DiscountVal: "",
      DiscountPer: "",
      MRP: "",
      DistributorPrice: "",
      Price: "",
    })
  }

  ngOnInit(): void {

    this.GetAllCustomers();
    this.GetAllProduct();
  }

  GetAllDistributor(productId: any) {
    this.cs.GetAllDistributorByProduct(productId).subscribe((users: any) => {
      if (users.ReturnObject !== null) {
        this.distributor = users.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  GetAllProduct() {
    this.cs.GetAllProduct().subscribe((product: any) => {
      if (product.ReturnObject !== null) {
        this.products = product.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  GetAllCustomers() {
    this.cs.GetAllCustomers().subscribe((Customer: any) => {
      if (Customer.ReturnObject !== null) {
        this.Customer = Customer.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }
  // GetAllSetting() {
  //   this.cs.GetAllCustomers().subscribe((Customer: any) => { })
  // }

  productChange(value: any) {
    this.MRP = value.MRP
    this.fg.patchValue({
      MRP: value.MRP
    });
    this.cs.GetByProductId(value.ProductId).subscribe((res: any) => {
      this.AllUnit = res.ReturnObject
      if (this.AllUnit[0].Units == 0) {
        this.cs.showSnackBar('danger', 'Product Not Available');
      } else {
        this.GetAllDistributor(value.ProductId);
      }
    });
  }
  distribtionChange(value: any) {
    this.cs.GetAllInventorysByProductId(this.fg.value.ProductId.ProductId, value.DistributorId).subscribe((res: any) => {
      this.fg.patchValue({
        DistributorPrice: res.ReturnObject[0].DistributorPrice
      });
    });
  }

  onclick() {
    var salses: any = this.fg.value;
    salses.ProductId = this.fg.value.ProductId.ProductId;
    salses.DistributorId = this.fg.value.DistributorId.DistributorId;
    salses.MRP = this.fg.value.MRP;
    this.cs.CreateSaleOrder(salses).subscribe((res: any) => {
      if (res.isError == 1) {
        this.cs.showSnackBar('danger', res.DetailsError);
      } else {
        this.cs.showSnackBar('success', 'Record Added successfully')
      }
      this.dialogRef.close();
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured : Record Not Created ');
    })
  }

  cancel() {
    this.dialogRef.close();
  }

  discountCal() {
    let discAmount = (Number(this.fg.value.DistributorPrice) * Number(this.fg.value.DiscountPer)) / 100;
    this.fg.patchValue({
      DiscountVal: discAmount,
      Price: this.fg.value.DistributorPrice - discAmount
    });
  }


}
