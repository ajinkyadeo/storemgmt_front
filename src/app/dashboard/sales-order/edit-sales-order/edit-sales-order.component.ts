import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-edit-sales-order',
  templateUrl: './edit-sales-order.component.html',
  styleUrls: ['./edit-sales-order.component.scss']
})
export class EditSalesOrderComponent implements OnInit {

  fg: FormGroup;
  distributor: any[] = [];
  products: any[] = [];
  sales: any[] = [];
  Customer: any[] = [];
  AllUnit: any[] = [];
  MRP: any = 0;
  sale: any;
  product: any;
  distributorobj: any;

  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1._id === o2._id;
  }
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router,
    private fb: FormBuilder, public dialogRef: MatDialogRef<EditSalesOrderComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.sale = this.data.value;
    debugger
    this.fg = this.fb.group({
      Id: '',
      CustomerId: '',
      ProductId: '',
      DistributorId: '',
      DiscountVal: "",
      DiscountPer: "",
      MRP: "",
      DistributorPrice: "",
      Price: "",
    })
  }

  ngOnInit(): void {

    this.GetAllCustomers();
    this.GetAllProduct();

    this.productChange(this.sale.MstProduct);
    this.distribtionChange(this.sale.MstDistributor)
    this.fg.patchValue({
      Id: this.sale.Id,
      CustomerId: this.sale.CustomerId,
      ProductId: this.sale.MstProduct,
      DistributorId: this.sale.MstDistributor,
      DiscountVal: this.sale.DiscountVal,
      DiscountPer: this.sale.DiscountPer,
      MRP: this.sale.MRP,
      Price: this.sale.Price,
    })




  }

  GetAllDistributor(productId: any) {
    this.cs.GetAllDistributorByProduct(productId).subscribe((users: any) => {
      if (users.ReturnObject !== null) {
        this.distributor = users.ReturnObject;

      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  GetAllProduct() {

    this.cs.GetAllProduct().subscribe((product: any) => {
      if (product.ReturnObject !== null) {
        this.products = product.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
    this.fg.patchValue({
      ProductId: this.sale.MstProduct,
    })
  }

  GetAllCustomers() {
    this.cs.GetAllCustomers().subscribe((Customer: any) => {
      if (Customer.ReturnObject !== null) {
        this.Customer = Customer.ReturnObject;
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }
  // GetAllSetting() {
  //   this.cs.GetAllCustomers().subscribe((Customer: any) => { })
  // }

  productChange(value: any) {
    debugger
    this.MRP = value.MRP
    this.fg.patchValue({
      MRP: value.MRP
    });
    this.cs.GetByProductId(value.ProductId).subscribe((res: any) => {
      this.AllUnit = res.ReturnObject
      if (this.AllUnit[0].Units == 0) {
        this.cs.showSnackBar('danger', 'Product Not Available');
      } else {
        this.GetAllDistributor(value.ProductId);
      }
    });
  }
  distribtionChange(value: any) {
    debugger
    this.cs.GetAllInventorysByProductId(this.fg.value.ProductId.ProductId, value.DistributorId).subscribe((res: any) => {
      this.fg.patchValue({
        DistributorPrice: res.ReturnObject[0].DistributorPrice
      });
    });
  }

  update() {
    debugger
    var salses: any = this.fg.value;
    salses.ProductId = this.fg.value.ProductId.ProductId;
    salses.DistributorId = this.fg.value.DistributorId.DistributorId;
    salses.MRP = this.fg.value.MRP;
    this.cs.UpdateSaleOrder(salses).subscribe((res: any) => {
      if (res == false) {
        this.cs.showSnackBar('danger', 'Record Not Updated');
      } else {
        this.cs.showSnackBar('success', 'Record Updated successfully')
      }
      this.dialogRef.close();
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured : Record Not Updated ');
    })
  }

  cancel() {
    this.dialogRef.close();
  }

  discountCal() {
    let discAmount = (Number(this.fg.value.DistributorPrice) * Number(this.fg.value.DiscountPer)) / 100;
    this.fg.patchValue({
      DiscountVal: discAmount,
      Price: this.fg.value.DistributorPrice - discAmount
    });
  }


}
