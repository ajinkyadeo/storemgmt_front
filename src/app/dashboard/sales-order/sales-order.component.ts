import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from '../../service/common.service';
import { AddSalesOrderComponent } from './add-sales-order/add-sales-order.component';
import { EditSalesOrderComponent } from './edit-sales-order/edit-sales-order.component';
@Component({
  selector: 'app-sales-order',
  templateUrl: './sales-order.component.html',
  styleUrls: ['./sales-order.component.scss']
})
export class SalesOrderComponent implements OnInit {


  value: string | undefined;

  state: string = "";
  user: any;
  sales: any[] = [];
  rolesPermission;
  displayedColumns: string[] = ['Id', 'CustomerName', 'ProductName', 'DistributorName', 'MRP', 'Dicount', 'DiscountPrice', 'Price', 'action'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false })
  sort!: MatSort;
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router) {
    // if (this.cs.userSession.Roles.RoleName != "Group Admin") {
    //   this.route.navigate(['dashboard']);
    // }
    var rolesMenu = JSON.parse(localStorage.roleMenu);
    this.rolesPermission = rolesMenu.filter((x: any) => x.menus.MenuName == "Sales Order")[0]
  }

  ngOnInit() {
    this.GetAllSaleOrders()
  }

  GetAllSaleOrders() {
    this.cs.GetAllSaleOrders().subscribe((sale: any) => {
      if (sale.ReturnObject !== null) {
        this.sales = sale.ReturnObject;
        this.patchMatchTable();
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  patchMatchTable() {

    this.dataSource = new MatTableDataSource(this.sales);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  editSales(row: any) {
    const dialogRef = this.dailog.open(EditSalesOrderComponent, {
      height: "430px",
      width: "600px",
      data: { value: row }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.GetAllSaleOrders();
    });

  }

  delete(row: any) {
    var obj = row;
    this.cs.GetDeleteSaleOrder(obj.Id).subscribe((res: any) => {
      if (res == true) {
        this.GetAllSaleOrders();
        this.cs.showSnackBar('success', 'Record romove successfully')
      } else {
        this.cs.showSnackBar('danger', 'Record can not be deleted');
      }
    })
  }


  addsales() {
    const dialogRef = this.dailog.open(AddSalesOrderComponent, {
      height: "430px",
      width: "600px",
    });
    dialogRef.afterClosed().subscribe(result => {
      this.GetAllSaleOrders();
    });
  }
}
