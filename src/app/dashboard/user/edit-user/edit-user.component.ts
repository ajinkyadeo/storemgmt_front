import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  user: any = null;
  users: any[] = [];
  // error_messege = error_messege;
  // action: string;
  // userdata;
  state: string = "add";
  role: any;
  roles: any[] = [];
  fg: FormGroup;
  maxNewDate = new Date();
  genders: any[] = [{ 'name': 'Male', 'value': 'Male' }, { 'name': 'Female', 'value': 'Female' }];
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
  constructor(public dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private cs: CommonService, private route: Router) {
    dialogRef.disableClose = true;
    this.maxNewDate.setUTCFullYear(this.maxNewDate.getUTCFullYear() - 5);
    // this.cs.fillSession();
    this.fg = this.fb.group({
      UserId: [''],
      RoleId: [''],
      FirstName: ['', [Validators.required, Validators.pattern('[A-Za-z ]*')]],
      LastName: ['', [Validators.required, Validators.pattern('[A-Za-z ]*')]],
      UserName: ['', [Validators.compose([Validators.required, Validators.pattern(this.emailPattern)])]],
      Gender: [''],
      DOB: [''],
      PhoneNumber: ['', [Validators.required, Validators.minLength, Validators.pattern(/^-?([0-9]\d*)?$/)]],
      Lock: [''],
      ModifiedBy: [''],

    });

  }

  get f() {
    return this.fg;
  }

  ngOnInit() {

    this.cs.GetAllRole().subscribe((roles: any) => {
      this.roles = roles.ReturnObject;
    })
    var id = this.cs.userSession[0].UserId
    debugger
    this.user = this.data.value;
    this.fg.patchValue({
      UserId: this.user.UserId,
      RoleId: this.user.RoleId,
      FirstName: this.user.FirstName,
      LastName: this.user.LastName,
      UserName: this.user.UserName,
      Gender: this.user.Gender,
      DOB: this.user.DOB,
      PhoneNumber: this.user.PhoneNumber,
      Lock: this.user.Lock,
      ModifiedBy: id,
    });
  }

  update() {
    debugger
    var user: any = this.fg.value;
    user.Gender.trim();
    this.cs.UpdateUser(user).subscribe((res: any) => {
      if (res == true) {
        this.cs.showSnackBar('success', 'User Record Updated successfully');
        this.dialogRef.close();
      } else {
        this.cs.showSnackBar('danger', 'User Record Not Updated successfully');
      }
    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured : User Record Not Updated');
    })
  }

  cancel() {
    this.dialogRef.close();
  }

}
