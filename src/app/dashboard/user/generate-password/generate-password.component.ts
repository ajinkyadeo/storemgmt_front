import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-generate-password',
  templateUrl: './generate-password.component.html',
  styleUrls: ['./generate-password.component.scss']
})
export class GeneratePasswordComponent implements OnInit {



  fg: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<GeneratePasswordComponent>, @Inject(MAT_DIALOG_DATA) public data: any
    , private fb: FormBuilder,
    private cs: CommonService) {
    data
    debugger
    var id = this.cs.userSession[0].UserId
    this.fg = this.fb.group({
      UserId: data.UserId,
      Password: data.Password,
      ModifiedBy: id
    })
  }

  ngOnInit() {

  }

  get f() {
    return this.fg;
  }

  onchangeProduct(event: any) {
  }

  submit() {
    var obj = this.fg.value;
    debugger
    this.cs.UpdatePassword(obj).subscribe((res: any) => {

      if (res == false) {
        this.cs.showSnackBar('danger', 'Password Not Generate');
      } else {
        this.cs.showSnackBar('success', 'Password Generate Successfully')
      }
      this.dialogRef.close();

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    })
  }

  cancel() {
    this.dialogRef.close();
  }

  generatePassword(passwordLength: any) {
    debugger;
    var numberChars = "0123456789";
    var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowerChars = "abcdefghijklmnopqrstuvwxyz";
    var allChars = numberChars + upperChars + lowerChars;
    var randPasswordArray = Array(passwordLength);
    randPasswordArray[0] = numberChars;
    randPasswordArray[1] = upperChars;
    randPasswordArray[2] = lowerChars;
    randPasswordArray = randPasswordArray.fill(allChars, 3);
    let newPass = this.shuffleArray(randPasswordArray.map(function (x) { return x[Math.floor(Math.random() * x.length)] })).join('');
    this.fg.patchValue({
      Password: newPass
    })
  }

  shuffleArray(array: any) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }



}
