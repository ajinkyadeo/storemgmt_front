import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from '../../service/common.service';
import { LockUnlockDialogComponent } from './lock-unlock-dialog/lock-unlock-dialog.component';
import { GeneratePasswordComponent } from './generate-password/generate-password.component';
import { EditUserComponent } from './edit-user/edit-user.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {


  value: string | undefined;

  state: string = "";
  user: any;
  users: any[] = [];
  rolesPermission: any;
  displayedColumns: string[] = ['Id', 'Role', 'FirstName', 'LastName', 'Email', 'PhoneNumber', 'Gender', 'Lock', 'action'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false })
  sort!: MatSort;
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router) {
    var rolesMenu = JSON.parse(localStorage.roleMenu);
    this.rolesPermission = rolesMenu.filter((x: any) => x.menus.MenuName == "User")[0]
  }


  ngOnInit() {
    this.Bindusers();
  }

  Bindusers() {
    this.cs.userlist().subscribe((users: any) => {
      if (users.ReturnObject !== null) {
        this.users = users.ReturnObject;
        this.patchMatchTable();
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });

  }

  patchMatchTable() {

    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  generatePassword(row: any) {
    const dialogRef = this.dailog.open(GeneratePasswordComponent, {
      width: '546px',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
      this.Bindusers();
    });
  }

  lockunlock(row: any) {
    const dialogRef = this.dailog.open(LockUnlockDialogComponent, {
      width: '546px',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
      this.Bindusers();
    });
  }

  edituser(row: any) {
    const dialogRef = this.dailog.open(EditUserComponent, {
      height: "430px",
      width: "600px",
      data: { value: row }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.Bindusers();
    });
  }

}
