import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-lock-unlock-dialog',
  templateUrl: './lock-unlock-dialog.component.html',
  styleUrls: ['./lock-unlock-dialog.component.scss']
})
export class LockUnlockDialogComponent implements OnInit {

  userId: any;
  isLock: any;
  status: any;
  constructor(
    public dialogRef: MatDialogRef<LockUnlockDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router, private cs: CommonService
  ) {
    this.userId = data.UserId
    this.isLock = data.Lock
    if (this.isLock == true) {
      this.status = "lock"
    } else {
      this.status = "Unlock"
    }
  }

  ngOnInit() {
  }

  submit() {
    this.cs.LockUser(this.userId, this.isLock).subscribe((res: any) => {

      this.dialogRef.close();

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    })
  }

  close() {
    this.dialogRef.close();
  }
}
