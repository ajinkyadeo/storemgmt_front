import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LockUnlockDialogComponent } from './lock-unlock-dialog.component';

describe('LockUnlockDialogComponent', () => {
  let component: LockUnlockDialogComponent;
  let fixture: ComponentFixture<LockUnlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LockUnlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LockUnlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
