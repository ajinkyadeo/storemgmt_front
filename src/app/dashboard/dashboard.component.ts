import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { CommonService } from '../service/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  state;
  fullname;
  roleMenu: any[] = [];
  @Output() public sidenavToggle = new EventEmitter();
  constructor(private route: Router, private cs: CommonService) {
    this.cs.fillSession();
    if (this.cs.userSession != null) {
      debugger
      this.fullname = this.cs.userSession[0].fullName;
    } else {
      this.route.navigate(['login']);
    }

    if (this.cs.userSession[0].MstRole.RoleName == "Owner") {
      this.state = "Owner";
    } else {
      this.state = "employee";
    }
  }
  ngOnInit() {
    this.GetRoleMenuByRoleId();
  }

  // GetAllRoleMenus() {
  //   this.cs.GetAllRoleMenus().subscribe((roleMenu: any) => {
  //     if (roleMenu.ReturnObject !== null) {
  //       this.roleMenu = roleMenu.ReturnObject;

  //     } else {
  //       this.cs.showSnackBar('danger', 'Data not found');
  //     }

  //   }, () => {
  //     this.cs.showSnackBar('danger', 'Error Occured');
  //   });
  // }

  GetRoleMenuByRoleId() {
    this.cs.GetRoleMenuByRoleId().subscribe((roleMenu: any) => {
      if (roleMenu.ReturnObject !== null) {
        this.roleMenu = roleMenu.ReturnObject;

        localStorage.roleMenu = JSON.stringify(this.roleMenu);
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  logout() {
    localStorage.clear();
    this.route.navigate(['login']);
  }
}
