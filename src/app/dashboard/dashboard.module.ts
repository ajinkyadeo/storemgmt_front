import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MaterialModule } from '../modules/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserComponent } from './user/user.component';
import { InventoryComponent } from './inventory/inventory.component';
import { CustomerComponent } from './customer/customer.component';
import { DistributorComponent } from './distributor/distributor.component';
import { ProductComponent } from './product/product.component';
import { SalesOrderComponent } from './sales-order/sales-order.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { AddInventoryComponent } from './inventory/add-inventory/add-inventory.component';
import { ProfileComponent } from './profile/profile.component';
import { LockUnlockDialogComponent } from './user/lock-unlock-dialog/lock-unlock-dialog.component';
import { GeneratePasswordComponent } from './user/generate-password/generate-password.component';
import { AddSalesOrderComponent } from './sales-order/add-sales-order/add-sales-order.component';
import { EditSalesOrderComponent } from './sales-order/edit-sales-order/edit-sales-order.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';


@NgModule({
  declarations: [
    DashboardComponent,
    UserComponent,
    InventoryComponent,
    CustomerComponent,
    DistributorComponent,
    ProductComponent,
    SalesOrderComponent,
    AddUserComponent,
    AddInventoryComponent,
    ProfileComponent,
    LockUnlockDialogComponent,
    GeneratePasswordComponent,
    AddSalesOrderComponent,
    EditSalesOrderComponent,
    EditUserComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    FlexLayoutModule
  ],
  entryComponents: [
    AddInventoryComponent,
    GeneratePasswordComponent,
    GeneratePasswordComponent,
    AddSalesOrderComponent,
    EditSalesOrderComponent
  ]
})
export class DashboardModule { }
