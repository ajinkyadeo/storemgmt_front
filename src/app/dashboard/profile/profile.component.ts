import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  value: string | undefined;

  state: string = "";
  user: any;
  users: any[] = [];
  displayedColumns: string[] = ['Id', 'FirstName', 'LastName', 'Email', 'PhoneNumber', 'Gender', 'Lock'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false })
  sort!: MatSort;
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router) {
    // if (this.cs.userSession.Roles.RoleName != "Group Admin") {
    //   this.route.navigate(['dashboard']);
    // }
  }


  ngOnInit() {
    this.Bindusers();
  }

  Bindusers() {
    var id = this.cs.userSession[0].UserId;
    this.cs.GetUserById(id).subscribe((users: any) => {
      if (users.ReturnObject !== null) {
        this.users = users.ReturnObject;
        this.patchMatchTable();
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });

  }

  patchMatchTable() {

    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
