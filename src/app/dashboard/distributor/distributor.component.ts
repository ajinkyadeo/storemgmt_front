import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.scss']
})
export class DistributorComponent implements OnInit {


  value: string | undefined;

  state: string = "";
  user: any;
  distributor: any[] = [];
  displayedColumns: string[] = ['Id', 'DistributorName', 'Code', 'PhoneNumber', 'Address', 'ContactPerson'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false })
  sort!: MatSort;
  constructor(private cs: CommonService, public dailog: MatDialog, private route: Router) {
    // if (this.cs.userSession.Roles.RoleName != "Group Admin") {
    //   this.route.navigate(['dashboard']);
    // }
  }

  ngOnInit() {
    this.GetAllDistributor();
  }

  GetAllDistributor() {
    this.cs.GetAllDistributor().subscribe((users: any) => {
      if (users.ReturnObject !== null) {
        this.distributor = users.ReturnObject;
        this.patchMatchTable();
      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });

  }

  patchMatchTable() {

    this.dataSource = new MatTableDataSource(this.distributor);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  // edituser(row: any) {
  //   const dialogRef = this.dailog.open(EdituserComponent, {
  //     height: "600px",
  //     width: "600px",
  //     data: { value: row }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     this.Bindusers();
  //   });

  // }

  delete(row: any) {
    // var obj = row;
    // this.cs.GetDeleteUserById(obj.Id).subscribe((res: any) => {
    //   if (res == true) {
    //     this.Bindusers();
    //     this.cs.showSnackBar('success', 'user romove successfully')
    //   } else {
    //     this.cs.showSnackBar('danger', 'user can not be deleted');
    //   }
    // })
  }

  // adduser() {
  //   const dialogRef = this.dailog.open(AdduserComponent, {
  //     height: "600px",
  //     width: "600px",
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     this.Bindusers();
  //   });
  // }


}
