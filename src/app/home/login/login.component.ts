import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg: FormGroup;
  userData: any;
  roleMenu: any[] = [];
  constructor(
    public route: Router,
    private fb: FormBuilder,
    private cs: CommonService,
  ) {
    this.fg = this.fb.group({
      userName: ['', [Validators.compose([Validators.required])]],
      password: ['', [Validators.required,]]
    })
  }

  ngOnInit() {
    this.GetRoleMenuByRoleId();
  }
  GetRoleMenuByRoleId() {
    debugger
    this.cs.GetRoleMenuByRoleId().subscribe((roleMenu: any) => {
      if (roleMenu.ReturnObject !== null) {
        this.roleMenu = roleMenu.ReturnObject;


      } else {
        this.cs.showSnackBar('danger', 'Data not found');
      }

    }, () => {
      this.cs.showSnackBar('danger', 'Error Occured');
    });
  }

  get f() {
    return this.fg;
  }

  login() {
    var obj = this.fg.value;
    localStorage.clear();
    this.cs.userlogin(obj.userName, obj.password).subscribe((res: any) => {
      this.userData = res.ReturnObject;
      if (this.userData == null) {
        this.cs.showSnackBar('danger', 'login faild : username or password is wrong');
      } else {
        localStorage.userSessionData = JSON.stringify(this.userData);
        localStorage.roleMenu = JSON.stringify(this.roleMenu);
        this.cs.userSession = JSON.parse(localStorage.userSessionData);
        this.route.navigate(['/dashboard/sales-order']);
        this.cs.showSnackBar('success', 'Login Successfully!');
      }
    }, error => {
      this.cs.showSnackBar('danger', 'Error Occured');
    })

  }
  register() {
    this.route.navigate(['register']);
  }

  forgotpassword() {
    this.route.navigate(['forgot-password']);
  }

}
